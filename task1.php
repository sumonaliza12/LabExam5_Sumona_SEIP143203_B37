<?php
class array_sort
{
    protected $_asort;

    public function __construct(array $asort)
    {
        //write additional code here
        $this->_asort = $asort;
    }
    //Type Additional Code Here To Write mySort method
    public function mySort()
    {
        $sorted = $this->_asort;
        sort($sorted);
        return $sorted;
    }
}
$sortarray = new array_sort(array(11, -2, 4, 35, 0, 8, -9));
print_r($sortarray->mySort())."<br>";
?>